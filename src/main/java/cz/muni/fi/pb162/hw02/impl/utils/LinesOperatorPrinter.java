package cz.muni.fi.pb162.hw02.impl.utils;


import java.util.List;

/**
 * Compute operations
 */
public class LinesOperatorPrinter {
    /**
     * Print size of line in format
     * @param line to format
     */
    public static void printSize(String line){
        System.out.println(String.format("%d: %s", line.length(), line));
    }

    /**
     * Print all minimal similar lines
     * @param lines to compute from
     */
    public static void printSimilar(List<String> lines){
       Levenshtein levenshtein = new Levenshtein(lines);
       System.out.println(String.format("Distance of %d", levenshtein.getMin()));
       List<String> finalLines = levenshtein.getLines();
       lines.subList(1,lines.size())
               .forEach(s -> System.out.println(String.format("%s ~= %s", finalLines.get(0),s)));


    }
}




