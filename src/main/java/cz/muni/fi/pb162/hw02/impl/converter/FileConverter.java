package cz.muni.fi.pb162.hw02.impl.converter;

import com.beust.jcommander.IStringConverter;
import cz.muni.fi.pb162.hw02.FileLoader;

import java.io.IOException;
import java.util.List;
// import java.util.logging.Level;
// import java.util.logging.Logger;

/**
 * Converter for file command line option
 */
public class FileConverter implements IStringConverter<List<String>> {

    @Override
    public List<String> convert(String path) {
        List<String> str = null;
        try {
             str = new FileLoader().loadAsLines(path);
        } catch (IOException e) {
            // Logger.getLogger(FileLoader.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            System.err.println(String.format("Unable to process file '%s'!",path));
        }
        return str;
    }
}
