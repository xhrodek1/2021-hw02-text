package cz.muni.fi.pb162.hw02.impl.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Class used for computing Levenshtein distance
 */
public class Levenshtein {
    private HashSet<String> similarLines;
    private Integer min;

    /**
     * Create Levenshtein object with lines
     * @param lines to check for similarity
     */
    public Levenshtein(List<String> lines) {
        this.similarLines =new HashSet<>();
        this.min = 0;
        compare(lines);
    }

    /**
     * Compute Levenshtein distance for 2 strings
     * @param s first string
     * @param t second string
     * @return distance

    private int distance(String s, String t){
        int lenS =  s.length();
        int lenT = t.length();
        int[] dist = IntStream.range(0,lenT+1).toArray();
        int n,m;
        for (int i = 1; i < lenS; i++) {
            dist[0] = i;
            n = i-1;
            for (int j = 1; j < lenT; j++) {
                m = Math.min(1 + Math.min(dist[j], dist[j - 1]), s.charAt(i-1) == t.charAt(j - 1) ? n : n + 1);
                n = dist[j];
                dist[j] = m;
            }

        }
        return dist[lenT];
    }
    */

    public static int distance(String s, String t) {
        int lenT = t.length();
        int [] dist = IntStream.range(0,lenT+1).toArray();
        int n,m;
        for (int i = 1; i <= s.length(); i++) {
            dist[0] = i;
            n = i - 1;
            for (int j = 1; j <= lenT; j++) {
                m = Math.min(1 + Math.min(dist[j], dist[j - 1]), s.charAt(i - 1) == t.charAt(j - 1) ? n : n + 1);
                n = dist[j];
                dist[j] = m;
            }
        }
        return dist[lenT];
    }
    /**
     * Compute Levenshtein distance for all strings
     * @param lines
     */
    private void compare(List<String> lines){
        int n;
        boolean add;
        for (int i = 0; i < lines.size(); i++) {
            add = false;
            for (int j = i + 1; j < lines.size(); j++) {
                n = distance(lines.get(i), lines.get(j));
                if (min == 0 || n < min) {
                    min = n;
                    this.similarLines.clear();
                    this.similarLines.add(lines.get(j));
                    add = true;

                } else if (min == n) {

                    this.similarLines.add(lines.get(j));
                    add = true;

                }
            }
            if (add){
                this.similarLines.add(lines.get(i));
            }
        }
    }
    public List<String> getLines() {
        return new ArrayList<>(similarLines);
    }

    public Integer getMin() {
        return min;
    }

}
