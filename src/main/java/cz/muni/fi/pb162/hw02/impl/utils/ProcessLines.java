package cz.muni.fi.pb162.hw02.impl.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Pre-process lines based on switches in args
 */
public class ProcessLines {
    private static List<String> lines;

    /**
     * Create line processor
     * @param linesList to create processor from
     */
    public ProcessLines(ArrayList<String> linesList){
        lines = linesList;
    }

    /**
     * Save only lines, that are unique
     */
    public ProcessLines unique(){
        lines = lines.stream().distinct().collect(Collectors.toList());
        return this;
    }

    /**
     * Make lines sorted
     */
    public ProcessLines sorted(){
        lines = lines.stream().sorted().collect(Collectors.toList());
        return this;
    }

    /**
     * Save only lines, that are duplicate
     */
    public ProcessLines duplicates(){
        HashSet<String> dist = new HashSet();
        lines = lines.stream().filter(s -> !dist.add(s)).collect(Collectors.toList());
        return this;
    }

    /**
     * Collect lines to list
     * @return List of lines
     */
    public List<String> getLines(){
        return lines;
    }

}
