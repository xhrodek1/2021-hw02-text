package cz.muni.fi.pb162.hw02.impl.validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.io.File;

/**
 * Validator for file command line option
 */
public class FileValidator implements IParameterValidator {
    /**
     * Check if file was loaded
     * @param name of the parameter
     * @param value of the parameter
     * @throws ParameterException if file is null or empty (check)
     */
    public void validate(String name, String value) throws ParameterException {
        File file = new File(value);
        if (!file.exists()) {
            throw new ParameterException(String.format("File %s doesn't exist.", value));
        }else if (!file.canRead()) {
            throw new ParameterException(String.format("File %s can't be read.", value));
        }
    }
}
