package cz.muni.fi.pb162.hw02.impl;

import com.beust.jcommander.Parameter;
import cz.muni.fi.pb162.hw02.TerminalOperation;
import cz.muni.fi.pb162.hw02.cmd.CommandLine;
import cz.muni.fi.pb162.hw02.cmd.TerminalOperationConverter;
import cz.muni.fi.pb162.hw02.impl.converter.FileConverter;
import cz.muni.fi.pb162.hw02.impl.utils.LinesOperatorPrinter;
import cz.muni.fi.pb162.hw02.impl.utils.ProcessLines;
import cz.muni.fi.pb162.hw02.impl.validators.FileValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Application class represents the command line interface of this application.
 * <p>
 * You are expected to implement  the  {@link Application#run(CommandLine)} method
 *
 * @author jcechace
 */
public class Application {

    @Parameter(names = "--help", help = true, description="Show basic usage information")
    private boolean showUsage = false;

    @Parameter(names = "--file", description="Path to readable text file",
            converter = FileConverter.class,
            //validateWith = FileValidator.class, // Lets pretend we do things the wrong way
            required = true)
    private ArrayList arrayLines;

    @Parameter(names = "-u", description="Get unique lines")
    private boolean unique;

    @Parameter(names = "-s", description="Get sorted lines")
    private boolean sort;

    @Parameter(names = "-d", description="Get duplicate lines")
    private boolean duplicates;

    @Parameter(converter = TerminalOperationConverter.class,
            description="SIZES | SIMILAR | COUNT | LINES \n Default option: LINES")
    private TerminalOperation operation = TerminalOperation.LINES;


    /**
     * Application entry point
     *
     * @param args command line arguments of the application
     */
    public static void main(String[] args) {
        Application app = new Application();

        CommandLine cli = new CommandLine(app);
        cli.parseArguments(args);

        if (app.showUsage || app.arrayLines == null) {
            cli.showUsage();
        } else if (app.duplicates && app.unique){
            System.err.println("Invalid combination of options was used!");
            cli.showUsage();
        } else {
            app.run(cli);
        }
    }

    /**
     * Application runtime logic
     *
     * @param cli command line interface
     */
    private void run(CommandLine cli) {
        ProcessLines text = new ProcessLines(arrayLines);
        if (unique){
            text.unique();
        } else if (duplicates){
            text.duplicates();
        }
        if (sort){
            text.sorted();
        }
        List<String> lines = text.getLines();
        switch(operation){
            case COUNT:
                System.out.println(lines.size());
                break;
            case SIZES:
                lines.forEach(LinesOperatorPrinter::printSize);
                break;
            case SIMILAR: //list
                LinesOperatorPrinter.printSimilar(text.unique().getLines());
                break;
            default:
                lines.forEach(System.out::println);
                break;
        }
    }
}
